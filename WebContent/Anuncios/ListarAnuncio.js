function onEditarAnuncio(id)
{
	if(id)
	{
		executarCommand("/br.com.faculdade/Controller?command=EditarAnuncio&id=" + id);
	}
	else
	{
		executarCommand("/br.com.faculdade/Controller?command=EditarAnuncio");
	}
}


function onDeletarAnuncio(id){
	if(confirm("Deseja deletar o anuncio?"))
	{
		executarCommand("/br.com.faculdade/Controller?command=DeletarAnuncio&id=" + id);
	}
}