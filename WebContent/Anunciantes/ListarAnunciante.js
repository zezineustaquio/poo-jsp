function onEditarAnunciante(id)
{
	if(id)
	{
		executarCommand("/br.com.faculdade/Controller?command=EditarAnunciante&id=" + id);
	}
	else
	{
		executarCommand("/br.com.faculdade/Controller?command=EditarAnunciante");
	}
}

function onDeletarAnunciante(id){
	if(confirm("Deseja deletar o anunciante?"))
	{
		executarCommand("/br.com.faculdade/Controller?command=DeletarAnunciante&id=" + id);
	}
}