function executarCommand(url){
	$("#br-com-faculdade-div-principal").load(url);
}

function executarCommandBody(url, dados)
{
	$.ajax({
		type: "POST",
		url: url,
		data: dados
	}).then(function(resultado){

		$("#br-com-faculdade-div-principal").html(resultado);
	});
}

$(document).ready(function(){
	$(".br-com-faculdade-menu-homes").on("click",  function(){ 
		$("#br-com-faculdade-div-principal").html("uma tela de boas vindas qualquer!");
	});

	$(".br-com-faculdade-menu-categorias").on("click", function(){
		executarCommand("/br.com.faculdade/Controller?command=ListarCategoria");
	});
	$(".br-com-faculdade-menu-anunciantes").on("click", function(){ 
		executarCommand("/br.com.faculdade/Controller?command=ListarAnunciante"); 
	});
	$(".br-com-faculdade-menu-anuncios").on("click",  function(){ 
		executarCommand("/br.com.faculdade/Controller?command=ListarAnuncio");
	});

	$(".br-com-faculdade-menu-usuarios").on("click",  function(){ 
		executarCommand("/br.com.faculdade/Controller?command=ListarUsuario");
	});

	$(".br-com-faculdade-menu-login").on("click",  function(){ 
		executarCommand("/br.com.faculdade/Controller?command=LoginUsuario");
	});

	$(".br-com-faculdade-menu-home").on("click", function(){
		executarCommand("/br.com.faculdade/Controller?command=MostrarAnuncio");
	});

	$(".br-com-faculdade-menu-logout").on("click", function(){
		$("body").load("/br.com.faculdade/Controller?command=LogoutUsuario");
	});
});

