<div>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand br-com-faculdade-menu-home" href="#">Publicidades</a>
			</div>

			<ul class="nav navbar-nav">
				<%
					if (request.getSession().getAttribute("usuarioLogado") != null) {
				%>
				<li><a class="br-com-faculdade-menu-categorias" href="#">Categorias</a></li>
				<li><a class="br-com-faculdade-menu-anunciantes" href="#">Anunciantes</a></li>
				<li><a class="br-com-faculdade-menu-anuncios" href="#">Anuncios</a></li>
				<li><a class="br-com-faculdade-menu-usuarios" href="#">Usu�rios</a></li>
				<li class="danger"><a class="br-com-faculdade-menu-logout" href="#">Sair</a></li>
				<%
					} else {
				%>
				<li><a class="br-com-faculdade-menu-login" href="#">Login</a></li>

				<%
					}
				%>
			</ul>

		</div>
	</nav>

	<script type="text/javascript">
		<jsp:include page="/Shared/menu.js" />
	</script>
</div>

