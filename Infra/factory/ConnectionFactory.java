package factory;

import java.sql.Connection;
import java.sql.SQLException;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class ConnectionFactory {

	private String driver = "com.mysql.jdbc.Driver";
	private String username = "aplicacao";
	private String password = "123456";

	private static ConnectionFactory connectionFactory = null;

	private ConnectionFactory() {
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public Connection getConnection() throws SQLException {
		MysqlDataSource dataSource = new MysqlDataSource();

		dataSource.setUseSSL(false);
		dataSource.setServerTimezone("America/Sao_Paulo");
		dataSource.setServerName("localhost");
		dataSource.setDatabaseName("faculdade");
		dataSource.setPortNumber(3306);
		dataSource.setUser(username);
		dataSource.setPassword(password);

		return dataSource.getConnection();

//		Connection conn = null;
//		try {
//			Properties properties = new Properties();
//			properties.put("username", username);
//			properties.put("password", password);
//			properties.put("allowPublicKeyRetrieval", Boolean.TRUE);
//			properties.put("useSSL", Boolean.FALSE);
//			conn = DriverManager.getConnection(connectionStr, properties);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return conn;
	}

	public static ConnectionFactory getInstance() {
		if (connectionFactory == null)
			connectionFactory = new ConnectionFactory();

		return connectionFactory;
	}
}
